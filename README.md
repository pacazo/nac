C++ implementation of Noughts and Crosses (a.k.a. Tic Tac Toe).

Features:
- Encapsulated in namespace nac.
- Class board only allows legal moves.
- Evaluation functions.
- Obtain all valid board states.
- Obtain all valid games.
- Small and efficient
- Pretty print boards and lists of boards to std::ostream.

Copyright 2019 Paco Arjonilla <pacoarjonilla@yahoo.es>
