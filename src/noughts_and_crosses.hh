/* NOUGHTS AND CROSSES
 *
 * (C) 2019-2022 Paco Arjonilla <pacoarjonilla@yahoo.es>
 */
#ifndef NOUGHTS_AND_CROSSES_HH
#define NOUGHTS_AND_CROSSES_HH

namespace nac {

enum piece : char
{
    NONE = ' ', // Empty
    O    = 'O',
    X    = 'X'
};

enum piece_position : unsigned char
{
	NW = 0, N = 1, NE = 2,
	W  = 3, C = 4, E  = 5,
	SW = 6, S = 7, SE = 8
};

enum outcome_t : char
{
    O_WINS  = 'O',
    X_WINS  = 'X',
    TIE     = 'T',
    PLAYING = 'P'
};

class board
{
private:
    std::vector<piece> raw_board = {9, NONE};

    template< template <typename...> typename container >
    friend std::ostream & ostream_put
                                (std::ostream &, const container< board > &);

    void transform(const char[9]);
public:
    // TODO (C++20): Use operator <=> which automatically generates all others.
    bool operator <  (board const& board2) const;
    bool operator == (board const& board2) const;

    // Query functions.
    outcome_t     outcome()                      const; // E.g.: if (!outcome())
    unsigned char turns_played()                 const;   //         play(S);
    piece         next_player()                  const;
    piece         get_square(piece_position pos) const;

    // State-changing functions.
    bool          play(piece_position pos); // True if play was successful.
    static board  play(const board & orig, piece_position pos); // When r-values
    void          reset();

    // Transformation functions.
    void rotate_left();
    void rotate_right();
    void rotate_180();
    void mirror_x();
    void mirror_y();
    void mirror_diag();
    void mirror_codiag();

    static board rotate_left   (const board & orig);
    static board rotate_right  (const board & orig);
    static board rotate_180    (const board & orig);
    static board mirror_x      (const board & orig);
    static board mirror_y      (const board & orig);
    static board mirror_diag   (const board & orig);
    static board mirror_codiag (const board & orig);

    // Special functions.
    unsigned unique_id() const;
    static constexpr unsigned id_max() { return 1<<18; }
    static std::set<board> all_valid_boards();
};

// Relational operators
//inline bool operator == (board const& nac_l, board const& nac_r) { return   nac_l == nac_r  ;}
inline   bool operator != (board const& nac_l, board const& nac_r) { return !(nac_l == nac_r) ;}
//inline bool operator <  (board const& nac_l, board const& nac_r) { return   nac_l <  nac_r  ;}
inline   bool operator >  (board const& nac_l, board const& nac_r) { return   nac_r  < nac_l  ;}
inline   bool operator <= (board const& nac_l, board const& nac_r) { return !(nac_r  < nac_l) ;}
inline   bool operator >= (board const& nac_l, board const& nac_r) { return !(nac_l <  nac_r) ;}

class game
{
    std::vector<piece_position> history;
    outcome_t outcome_cache;

public:
    game(std::vector<piece_position> & permutation);

    bool operator < (const game & g2) const;

    outcome_t outcome() const;
    std::vector<board> decompose() const;

    static std::set<game> all_valid_games();
};




} // namespace nac;

// Formatted output
std::ostream & operator << (std::ostream &, const nac::piece &);
std::ostream & operator << (std::ostream &, const nac::board &);
std::ostream & operator << (std::ostream &, const std::vector<nac::board> &);
std::ostream & operator << (std::ostream &, const std::set   <nac::board> &);



#endif
