/* NOUGHTS AND CROSSES
 *
 * (C) 2019 Paco Arjonilla <pacoarjonilla@yahoo.es>
 */
#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

import nac;

bool failed = false;

#define TEST(X) failed |= !(X)

int
main()
{
    using namespace nac;
    std::set<board> bds = board::all_valid_boards();
    std::cout << std::endl << "Number of valid boards: "
        << bds.size() << std::endl;

    std::cout << "Unique ID of the last game: "
        << bds.rbegin()->unique_id() << std::endl;

    int winsO = 0, winsX = 0, ties = 0, playing = 0;

    for (auto & board : bds) {
	switch (board.outcome()) {
	    case O_WINS:  winsO++;   break;
	    case X_WINS:  winsX++;   break;
	    case TIE:     ties++;    break;
	    case PLAYING: playing++; break;
	}
    }
    std::cout << "Wins O:  " << winsO   << std::endl;
    std::cout << "Wins X:  " << winsX   << std::endl;
    std::cout << "Ties:    " << ties    << std::endl;
    std::cout << "Playing: " << playing << std::endl;

    // Transformation tests.
    std::cout << std::endl
        << "Transformations (left, right, 180, x, y, diag, codiag) -->"
        << std::endl;
    board transf;
    transf.play(NW);
    transf.play(N);
    std::vector<board> tr_tests;
    tr_tests.reserve(20);
    tr_tests.push_back(transf); transf.rotate_left();
    tr_tests.push_back(transf); transf.rotate_right();
    tr_tests.push_back(transf); transf.rotate_180();
    tr_tests.push_back(transf); transf.mirror_x();
    tr_tests.push_back(transf); transf.mirror_y();
    tr_tests.push_back(transf); transf.mirror_diag();
    tr_tests.push_back(transf); transf.mirror_codiag();
    tr_tests.push_back(transf);
    std::cout << tr_tests;

    auto partidas = game::all_valid_games();

    std::cout << std::endl << "Number of distinct games: "
        << partidas.size() << std::endl;

    winsO = 0, winsX = 0, ties = 0, playing = 0;

    for (auto & partida : partidas)
	switch (partida.outcome())
	{

	    case O_WINS:  winsO++;   break;
	    case X_WINS:  winsX++;   break;
	    case TIE:     ties++;    break;
	    case PLAYING: playing++; break;
	}

    std::cout << "Wins O:  " << winsO   << std::endl;
    std::cout << "Wins X:  " << winsX   << std::endl;
    std::cout << "Ties:    " << ties    << std::endl;
    std::cout << "Playing: " << playing << std::endl;

    TEST(true);

    return failed;
}
