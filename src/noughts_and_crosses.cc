/* NOUGHTS AND CROSSES
 *
 * (C) 2019 Paco Arjonilla <pacoarjonilla@yahoo.es>
 */
module;

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

export module nac;


export {
#include "noughts_and_crosses.hh"
}

namespace nac {


const std::vector<piece_position> all_positions = {NW, N, NE, W, C, E, SW, S, SE};

/************** BOARD METHODS **************/

bool
board::operator < (board const& board2) const
{
    for (int i = 8; i >= 0; i--) {
	if ( raw_board[i] < board2.raw_board[i] )
	    return true;
	else if ( raw_board[i] > board2.raw_board[i] )
	    return false;
    }
    return false;
}

bool
board::operator == (board const& board2) const
{
    for (int i = 0; i < 9; i++) {
	if ( raw_board[i] != board2.raw_board[i] )
	    return false;
    }
    return true;
}

outcome_t
board::outcome() const
{
    unsigned char win_map[8][3] = {
	  {0, 1, 2},
	  {3, 4, 5},
	  {6, 7, 8},
	  {0, 3, 6},
	  {1, 4, 7},
	  {2, 5, 8},
	  {0, 4, 8},
	  {2, 4, 6} };

    for (int j = 0; j < 8; j++) {
	piece ref = raw_board[ win_map[j][0] ];
	if ( ( ref != NONE )
		&& ( ref == raw_board[ win_map[j][1] ] )
		&& ( ref == raw_board[ win_map[j][2] ] ))
	    return ref == O ? O_WINS : X_WINS;
    }
    // Otherwise
    return (turns_played() == 9) ? TIE : PLAYING;
}

unsigned char
board::turns_played() const
{
    unsigned char retval = 0;
    for (int i = 0; i < 9; i++)
	if (raw_board[i] != NONE)
	    retval++;
    return retval;
}

piece
board::next_player() const
{
    if (outcome() == PLAYING)
	return (turns_played() % 2) ? X : O;
    else
	return NONE;
}

piece
board::get_square(piece_position pos) const
{
    return raw_board[pos];
}

board board::play(const board & orig, piece_position pos)
{
    board ret = orig;
    ret.play(pos);
    return ret;
}

bool
board::play(piece_position pos)
{
    piece next = next_player();
    if (outcome() == PLAYING && next != NONE && raw_board[pos] == NONE) {
	raw_board[pos] = next;
	return true;
    }
    else
	return false;
}

void
board::reset()
{
    raw_board = {9, NONE};
}

void board::transform(const char permutation_indexes[9])
{
    std::vector<piece> new_board(9);
    for (int i = 0; i < 9; i++)
	new_board[ permutation_indexes[i] ] = raw_board[i];
    raw_board = new_board;
}

/* Board positions 0-index:
 *
 *    0 ┃ 1 ┃ 2
 *   ━━━╋━━━╋━━━
 *    3 ┃ 4 ┃ 5
 *   ━━━╋━━━╋━━━
 *    6 ┃ 7 ┃ 8
 */

const char idx_rotate_left[9]   = {6, 3, 0, 7, 4, 1, 8, 5, 2};
const char idx_rotate_right[9]  = {2, 5, 8, 1, 4, 7, 0, 3, 6};
const char idx_rotate_180[9]    = {8, 7, 6, 5, 4, 3, 2, 1, 0};
const char idx_mirror_x[9]      = {6, 7, 8, 3, 4, 5, 0, 1, 2};
const char idx_mirror_y[9]      = {2, 1, 0, 5, 4, 3, 8, 7, 6};
const char idx_mirror_diag[9]   = {0, 3, 6, 1, 4, 7, 2, 5, 8};
const char idx_mirror_codiag[9] = {8, 5, 2, 7, 4, 1, 6, 3, 0};

void board::rotate_left()    { transform(idx_rotate_left); }
void board::rotate_right()   { transform(idx_rotate_right); }
void board::rotate_180()     { transform(idx_rotate_180); }
void board::mirror_x()       { transform(idx_mirror_x); }
void board::mirror_y()       { transform(idx_mirror_y); }
void board::mirror_diag()    { transform(idx_mirror_diag); }
void board::mirror_codiag()  { transform(idx_mirror_codiag); }

board board::rotate_left   (const board & orig) { board ret = orig; ret.rotate_left();   return ret; }
board board::rotate_right  (const board & orig) { board ret = orig; ret.rotate_right();  return ret; }
board board::rotate_180    (const board & orig) { board ret = orig; ret.rotate_180();    return ret; }
board board::mirror_x      (const board & orig) { board ret = orig; ret.mirror_x();      return ret; }
board board::mirror_y      (const board & orig) { board ret = orig; ret.mirror_y();      return ret; }
board board::mirror_diag   (const board & orig) { board ret = orig; ret.mirror_diag();   return ret; }
board board::mirror_codiag (const board & orig) { board ret = orig; ret.mirror_codiag(); return ret; }

// Uses 2 bits for each board position to describe uniquely the board.
unsigned board:: unique_id() const
{
    unsigned id = 0;

    for (auto pos : raw_board)
    {
        id <<= 2;

        if (pos == NONE)
            id += 0;

        else if (pos == O)
            id += 1;

        else if (pos == X)
            id += 2;
    }

    return id;
}

std::set<board>
board::all_valid_boards()
{
    std::set<board> all_boards;

    // The unplayed board is valid.
    all_boards.emplace( );

    // There are 9 spots to fill.
    for (int spot = 0; spot < 9; spot++) {

	// For each previously-calculated valid board,
	for (auto & start : all_boards) {

	    // all spots are played once.
	    for (auto turn : all_positions) {

		all_boards.insert(play(start, turn));

    }   }   }

    return all_boards;
}


/************** GAME METHODS **************/

game::game(std::vector<piece_position> & permutation)
{
    board sandbox;
    auto turn = permutation.begin();
    while (turn != permutation.end()) {
	if (!sandbox.play(*(turn++))) {
	    turn--;
	    break;
	}
    }
    history = {permutation.begin(), turn};
    outcome_cache = sandbox.outcome();
}

outcome_t
game::outcome() const
{
    return outcome_cache;
}

std::vector<board>
game::decompose() const
{
    using namespace std;
    board playing;
    std::vector<board> retval(1,playing);
    for (auto turn : history) {
	playing.play(turn);
	retval.push_back(playing);
    }
    return retval;
}

bool
game::operator < (const game & g2) const
{
    return (history < g2.history);
}

std::set< game >
game::all_valid_games()
{
    std::set< game > ret_games;
    auto turns = all_positions;

    do
	ret_games.emplace(turns);

    while (std::next_permutation(turns.begin(), turns.end()));


    return ret_games;
}

} // namespace nac

/************** FORMATTED OUTPUT **************/

std::ostream &
operator << (std::ostream & out, const nac::piece & arg)
{
    return out << static_cast<char>(arg);
}

std::ostream &
operator << (std::ostream& out, const nac::board & arg)
{
    std::vector<nac::board> l(1, arg);
    return out << l;
}

template< template <typename...> typename container >
std::ostream &
nac::ostream_put (std::ostream & out, const container< nac::board > & board_list)
{
    using namespace nac;

    const std::string h = "\u2501";
    const std::string v = "\u2503";
    const std::string x = "\u254B";
    const std::string s = " ";

    out << std::endl;

    for (auto board : board_list) // Row 1
	out << "   " << s << board.raw_board[NW] << s
	        << v << s << board.raw_board[N]  << s
	        << v << s << board.raw_board[NE] << s;
    out << std::endl;

    for (size_t i = 0; i < board_list.size(); i++)
	out << "   " << h << h << h << x << h << h << h << x << h << h << h;
    out << std::endl;

    for (auto board : board_list) // Row 2
	out << "   " << s << board.raw_board[W] << s
	        << v << s << board.raw_board[C] << s
	        << v << s << board.raw_board[E] << s;
    out << std::endl;

    for (size_t i = 0; i < board_list.size(); i++)
	out << "   " << h << h << h << x << h << h << h << x << h << h << h;
    out << std::endl;

    for (auto board : board_list) // Row 3
	out << "   " << s << board.raw_board[SW] << s
	        << v << s << board.raw_board[S]  << s
	        << v << s << board.raw_board[SE] << s;
    out << std::endl;

    for (auto board : board_list) { // Information
	std::string info;
	switch (board.outcome()) {
	    case O_WINS:  info = " O wins"; break;
	    case X_WINS:  info = " X wins"; break;
	    case TIE:     info = "  TIE  "; break;
	    case PLAYING: info = "       "; break;
	}
	out << "   " << s << s << info << s << s;
    }

    out << std::endl;

    return out;
}

std::ostream &
operator << (std::ostream & out, const std::vector<nac::board> & board_list)
{
    return ostream_put(out, board_list);
}

std::ostream &
operator << (std::ostream & out, const std::set   <nac::board> & board_list)
{
    return ostream_put(out, board_list);
}

